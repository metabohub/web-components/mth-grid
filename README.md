# mth-grid (BETA VERSION)

[![pipeline status](https://forgemia.inra.fr/metabohub/web-components/mth-grid/badges/master/pipeline.svg)](https://forgemia.inra.fr/metabohub/web-components/mth-grid/-/commits/master)
[![npm package](https://img.shields.io/gitlab/v/tag/metabohub%2Fweb-components%2Fmth-grid?gitlab_url=https%3A%2F%2Fforgemia.inra.fr%2F)](https://forgemia.inra.fr/metabohub/web-components/mth-grid/-/packages)


The storybook documentation is available [here](https://metabohub.pages.mia.inra.fr/web-components/mth-grid/).

## Types

| Name | Value |
| --- | --- |
| `MthGridHeader` | `{ title: string, key: string, sortable?: boolean, searchable?: boolean, chip?: boolean, chipColors?: { [x: string]: string }, editable?: boolean, vHtml?: boolean, selected?: boolean, width?: number, sort?: (a, b) => number }` |
| `MthGridItem` | `{ [x: string]: string \| boolean \| number }` |
| `SelectStrategy` | `"single" \| "page" \| "all"` |

## Props

| Name       | Description           | Type            | Default |
| ---------- | --------------------- | --------------- | ------- |
| `headers`             | header of the table with `title`: name of the column ; `key`: matching value of `items` property ; `sortable`: if the column is sortable ; and `searchable`: if you include a searchbar for the column            | `MthGridHeader[]` | —       |
| `items`               | content of the table with `x` matching the `key` property of      | `MthGridItem[]`        | —       |
| `selectStrategy?`     | the type of select for the rows : single or multiple | `SelectStrategy` | `undefined` |
| `search?`             | display a search bar to search in the table, either in all the fields or one in particular (headers having the `searchable` property set to true)  | `boolean`    | `true`       |
| `isLoading?`          | if the table is loading the data or not    | `boolean`              | `false`       |
| `noColumnParameters?` | boolean to remove the dialog parameters to rename and change the order of the columns | `boolean` | `false` |
| `noDownloadButton?`   | boolean to remove the button to download the content of the grid | `boolean` | `false` |
| `itemSelectKey?`      | property for which the selection feature is looking for to differentiate the rows | `string` | `"id"` |

## Emits

| Name        | Description                                                                               | Type of `$event`    |
| ----------- | ----------------------------------------------------------------------------------------- | ------------------- |
| `selection` | triggered when the list of currently selected rows changes | `MthGridItem[]` |
| `selectedHeaders` | the headers currently displayed in the table | `MthGridHeader[]` |

## Slots

| Name      | Description                        | Data |
| --------- | ---------------------------------- | ---- |
| `actions` | action buttons to add for each row | `{ itemAction: MthGridItem }`

## Use example

This web component requires [Vuetify](https://vuetifyjs.com/en/).

```html
<template>
  <mth-grid
    :headers
    :items
  >
    <template #actions="{ itemAction }">
        <button @click="console.log(itemAction.a)">Info</button>
    </template>
  </mth-grid>
</template>

<script setup lang="ts">
import { MthGrid } from "@metabohub/mth-grid";  // import component
import "@metabohub/mth-grid/dist/style.css";   // import style
import type { MthGridHeader, MthGridItem } from "@metabohub/mth-grid";

const headers = ref<MthGridHeader[]>([
  { title: "A", key: a }
]);
const items = ref<MthGridItem[]>([
  { a: 1 }
]);
</script>
```

### Columns with v-chips

```ts
const headers = ref<MthGridHeader[]>([
  { title: "A", key: a, chip: true, colorChips: { "true": "green", "false": "red" } }
]);
const items = ref<MthGridItem[]>([
  { a: "true" }, // green
  { a: "false" }, // red
  { a: "other" } // grey
]);
```

### Editable columns

```ts
const headers = ref<MthGridHeader[]>([
  { title: "A", key: a, editable: true }
]);
const items = ref<MthGridItem[]>([
  { a: "something editable" }
]);
```

### v-html columns

```ts
const headers = ref<MthGridHeader[]>([
  { title: "A", key: a, vHtml: true }
]);
const items = ref<MthGridItem[]>([
  { a: "<i>italic</i>" }
]);
```

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

Check the coverage of the unit tests :
```sh
npm run coverage
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

### View documentation

```sh
npm run storybook
```

## CICD pipeline

### Tests

```yml
test:
  image: node:latest
  stage: test
  before_script:
    - npm install
  script:
    - npm run test:unit
```

This runs the unit tests defined in `src/components/__tests__/MthGrid.spec.ts`

### Deploy

```yml
.publish:
  stage: deploy
  before_script:
    - apt-get update && apt-get install -y git default-jre
    - npm install
    - npm run build
    - npm run chromatic
```

This builds the component as an npm package, and publish the story in chromatic, a website to view the storybook.
