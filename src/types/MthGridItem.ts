export type MthGridItem = {
  [x: string]: string | number;
};
