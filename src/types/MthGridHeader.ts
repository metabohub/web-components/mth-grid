export type MthGridHeader = {
  title: string;
  key: string;
  sortable?: boolean;
  searchable?: boolean;
  // display the column content in chips
  chip?: boolean;
  chipColors?: { [x: string]: string };
  // be able to edit the values of the column
  editable?: boolean;
  // display the column in a v-html tag (useful for <sub> etc)
  vHtml?: boolean;
  // by default: the column is displayed
  selected?: boolean;
  width?: number;
  // custom sort function
  sort?: (a, b) => number;
};
