import { describe, expect, test } from "vitest";
import { getHeadersList, clearList, renameAndReorderHeaders, findClosestMatchInArray } from "@/composables/utils";
import type { MthGridHeader } from "@/types/MthGridHeader";

describe("getHeaders", () => {
  test("should return empty lists when headers is empty", () => {
    const headers: MthGridHeader[] = [];
    const { chipHeaders, editableHeaders } =
      getHeadersList(headers);

    expect(chipHeaders).toEqual([]);
    expect(editableHeaders).toEqual([]);
  });

  test("should return the correct lists when headers contain vchip, editable and v-html columns", () => {
    const headers: MthGridHeader[] = [
      { title: "col1", key: "id", searchable: false },
      { title: "col2", key: "name", searchable: true, editable: true },
      {
        title: "col3",
        key: "bool",
        searchable: false,
        chip: true,
        chipColors: { red: "red", green: "green" },
      },
      { title: "col5", key: "chemical", searchable: false, vHtml: true },
    ];
    const { chipHeaders, editableHeaders, vHtmlHeaders } =
      getHeadersList(headers);

    expect(chipHeaders).toEqual(["bool"]);
    expect(editableHeaders).toEqual(["name"]);
    expect(vHtmlHeaders).toEqual(["chemical"]);
  });

  test("should return empty lists when headers do not contain vchip, editable and v-html columns", () => {
    const headers: MthGridHeader[] = [
      { title: "col1", key: "id", searchable: false },
      { title: "col2", key: "name", searchable: true },
      { title: "col3", key: "bool", searchable: false },
    ];
    const { chipHeaders, editableHeaders, vHtmlHeaders } =
      getHeadersList(headers);

    expect(chipHeaders).toEqual([]);
    expect(editableHeaders).toEqual([]);
    expect(vHtmlHeaders).toEqual([]);
  });
});

describe("clearList", () => {
  test("should clear the given list", () => {
    const list = ["a", "b", "c"];
    clearList(list);

    expect(list).toEqual([]);
  });
});

describe("renameAndReorderHeaders", () => {
  test("should return the rearranged and renamed headers", () => {
    const h1: MthGridHeader[] = [
      { title: "Name", key: "name" },
      { title: "Ids", key: "ids" },
      { title: "Formula", key: "formula" },
      { title: "Smiles", key: "smiles" },
      { title: "Chebi", key: "chebi" },
      { title: "Actions", key: "actions" },
    ];
    const h2: MthGridHeader[] = [
      { title: "Identifiers", key: "ids" },
      { title: "Name", key: "name" },
      { title: "Chemical Formula", key: "formula" },
      { title: "Actions", key: "actions" },
    ];
    const result = renameAndReorderHeaders(h1, h2);

    expect(result.length).toEqual(h1.length);
    expect(result[0]).toEqual({ title: "Identifiers", key: "ids" });
    expect(result[1]).toEqual({ title: "Name", key: "name" });
    expect(result[2]).toEqual({ title: "Chemical Formula", key: "formula" });
    expect(result[3]).toEqual({ title: "Actions", key: "actions" });
    expect(result[4]).toEqual({ title: "Smiles", key: "smiles" });
    expect(result[5]).toEqual({ title: "Chebi", key: "chebi" });
  });
});

describe("findClosestMatchInArray", () => {
  test("should return the closest match in the array", () => {
    const array = [1, 4, 10, 25, 7, 98, 15];
    const result = findClosestMatchInArray(8, array);

    expect(result.closestMatch).toEqual(7);
    expect(result.diff).toEqual(1);
  });
  test("should return the closest match in the array", () => {
    const array = [1, 4, 10, 25, 7, 98, 15];
    const result = findClosestMatchInArray(78, array);

    expect(result.closestMatch).toEqual(98);
    expect(result.diff).toEqual(20);
  });
  test("should return the closest match in the array", () => {
    const array = [1, 4, 10, 25, 7, 98, 15];
    const result = findClosestMatchInArray(-5, array);

    expect(result.closestMatch).toEqual(1);
    expect(result.diff).toEqual(6);
  });

  test("should return the first match", () => {
    const array = [1, 4, 10, 25, 7, 98, 15];
    const result = findClosestMatchInArray(20, array);

    expect(result.closestMatch).toEqual(25);
    expect(result.diff).toEqual(5);
  });
});
