import type { MthGridHeader } from "@/types/MthGridHeader";

/**
 * Checks the attribute in the headers to create lists of header keys for vchip columns, and editable columns
 * @param headers - The list of the grid's headers.
 * @returns An object containing the lists of columns of type 'vchip', and 'editable'.
 */
export function getHeadersList(headers: MthGridHeader[]): {
  chipHeaders: string[];
  editableHeaders: string[];
  vHtmlHeaders: string[];
} {
  const chipHeaders: string[] = [];
  const editableHeaders: string[] = [];
  const vHtmlHeaders: string[] = [];

  headers.forEach((h: MthGridHeader) => {
    if (h.chip) {
      chipHeaders.push(h.key);
    }

    if (h.editable) {
      editableHeaders.push(h.key);
    }

    if(h.vHtml) {
      vHtmlHeaders.push(h.key);
    }
  });
  return { chipHeaders, editableHeaders, vHtmlHeaders };
}

/**
 * Clears the given list.
 * @param l - The list to clear.
 */
export function clearList(l: Array<string>) {
  while (l.length > 0) {
    l.pop();
  }
}

/**
 * Updates value and order of headers with the new headers
 * @param headers a list of headers
 * @param newHeaders a sublist of headers that were renamed and rearrnged
 * @returns the rearranged and renamed headers
 */
export function renameAndReorderHeaders(headers: MthGridHeader[], newHeaders: MthGridHeader[]): MthGridHeader[] {
  // for each header in the new headers
  newHeaders.forEach((newHeader: MthGridHeader, index: number) => {
    // find the key in oldHeaders and remove it the headers
    const oldIndex = headers.findIndex((h: MthGridHeader) => h.key === newHeader.key);
    headers.splice(oldIndex, 1);
    // add it in the headers at the index of the new header
    headers.splice(index, 0, newHeader);
  });

  return headers;
}

/**
 * Finds the closest match to a number in an array
 * @param n - The number to find the closest match to.
 * @param array - The array to search.
 * @returns An object containing the difference between the number and the closest match, and the closest match.
 */
export function findClosestMatchInArray(n: number, array: Array<number>): { diff: number, closestMatch: number } {
  let closestMatch = array[0];
  let diff = Math.abs(n - closestMatch);
  array.forEach(i => {
    const newdiff = Math.abs(n - i);
    if (newdiff < diff) {
      diff = newdiff;
      closestMatch = i;
    }
  });

  return { diff, closestMatch };
}
