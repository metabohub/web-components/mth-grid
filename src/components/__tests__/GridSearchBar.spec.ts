import { describe, expect, test } from "vitest";
import { mount } from "@vue/test-utils";
import { createVuetify } from "vuetify";
import GridSearchBar from "../GridSearchBar.vue";
import { MthSearchBar } from "@metabohub/mth-search-bar";

function columnsFormdWrapper(searchableHeadersTitles: string[]) {
  const vuetify = createVuetify();
  return mount(GridSearchBar, {
    global: {
      plugins: [vuetify],
    },
    props: {
        searchableHeadersTitles,
      dark: false,
    },
  });
}

const titles: string[] = ["Col 1", "Col 2"];

describe("GridSearchBar.vue", () => {
  test("should render", () => {
    const wrapper = columnsFormdWrapper(titles);

    // v-select
    expect(wrapper.find("[data-testid='searchVSelect']").exists).toBeTruthy();
    // search bar
    expect(wrapper.find("[data-testid='globalSearch']").exists()).toBeTruthy();
  });

  test("should select 'All' by default", () => {
    const wrapper = columnsFormdWrapper(titles);

    // get v-select
    const vselectTitles = wrapper.find("[data-testid='searchVSelect'] .v-select__selection-text");
    expect(vselectTitles.element.textContent).toContain("All");
  });

  test("writing in search bar should emit an event", () => {
    const wrapper = columnsFormdWrapper(titles);

    // mock the writing in search bar
    wrapper.getComponent(MthSearchBar).vm.$emit("search", {
      searchInput: "mario",
      searchType: "EXACT",
    });
    
    // check that the event has been emitted
    expect(wrapper.emitted("submit")).toBeTruthy();
    expect(wrapper.emitted("submit")![0][0]).toEqual({ searchValue: "mario", searchType: "EXACT", searchField: "All" });
  });
});
