import { SelectStrategy } from "./../../types/SelectStrategy";
import { describe, expect, test, vi } from "vitest";
import { flushPromises, mount } from "@vue/test-utils";
import { createVuetify } from "vuetify";
import MthGrid from "@/components/MthGrid.vue";
import { MthSearchBar } from "@metabohub/mth-search-bar";

import type { MthGridItem } from "@/types/MthGridItem.js";
import type { MthGridHeader } from "@/types/MthGridHeader.js";

// Mock the ResizeObserver
const ResizeObserverMock = vi.fn(() => ({
  observe: vi.fn(),
  unobserve: vi.fn(),
  disconnect: vi.fn(),
}));

// Stub the global ResizeObserver
vi.stubGlobal("ResizeObserver", ResizeObserverMock);

function mthGridWrapper(
  headers: MthGridHeader[],
  items: MthGridItem[],
  slotAction = "",
  search?: boolean,
  selectStrategy?: SelectStrategy
) {
  const isLoading = false;
  const vuetify = createVuetify();
  return slotAction !== ""
    ? mount(MthGrid, {
        global: {
          plugins: [vuetify],
        },
        props: {
          headers,
          items,
          selectStrategy,
          search,
          isLoading,
        },
        slots: {
          actions: slotAction,
        },
      })
    : mount(MthGrid, {
        global: {
          plugins: [vuetify],
        },
        props: {
          headers,
          items,
          selectStrategy,
          search,
          isLoading,
        },
      });
}

// DATA
const headers: MthGridHeader[] = [
  { title: "col1", key: "id", searchable: false },
  { title: "col2", key: "name", searchable: true, editable: true },
  {
    title: "col3",
    key: "bool",
    searchable: false,
    chip: true,
    chipColors: { red: "red", green: "green" },
  },
  { title: "col4", key: "tags", searchable: false, vHtml: true }
];
const items: MthGridItem[] = [
  { id: "Id1", name: "name1", bool: "green", tags: "<i>italic</i>" },
  { id: "mario", name: "name2", bool: "nothing", tags: "<b>bold</b>" },
  { id: "Id3", name: "mario", bool: "red", tags: "CO<sub>2</sub>" },
];

describe("MthGrid.vue ------ display global elements", () => {
  test("should display a table with the right number of columns and rows", async () => {
    const wrapper = mthGridWrapper(headers, items);
    await flushPromises();

    // count columns and rows
    expect(wrapper.find("table").exists()).toBe(true);
    expect(wrapper.findAll("th").length).equals(headers.length);
    expect(wrapper.findAll("tbody > tr").length).equals(items.length);
  });
  test("should display additional action columns with buttons inside", async () => {
    const slot =
      '<template #actions="item">' +
      '<div class="operation-wrapper">' +
      '<button @click="id(item.rowId)"> Info </button>' +
      '<button @click="name(item.rowId)"> Other operation </button>' +
      "</div>" +
      "</template>";
    const wrapper = mthGridWrapper(headers, items, slot);
    await flushPromises();

    // count columns and rows
    expect(wrapper.findAll("th").length).equals(headers.length + 1); // Actions columns
    expect(wrapper.findAll("tbody > tr").length).equals(items.length);
    expect(wrapper.findAll(".operation-wrapper").length).equals(items.length);
  });
  test("should display a checkbox column", async () => {
    const wrapper = mthGridWrapper(headers, items, "", true, "all");
    await flushPromises();

    // count columns and rows
    expect(wrapper.find("table").exists()).toBe(true);
    expect(wrapper.findAll("th").length).equals(headers.length + 1); // checkbox column
    expect(wrapper.findAll("tbody > tr").length).equals(items.length);
    // count checkboxes
    expect(wrapper.findAll("input[type='checkbox']").length).equals(items.length + 1); // select all
  });
});

describe("MthGrid.vue ------ types of columns", () => {
  test("should display the boolean column as v-chips", async () => {
    const wrapper = mthGridWrapper(headers, items);
    await flushPromises();

    // count v-chips
    expect(wrapper.findAll("tbody .v-chip").length).equals(items.length);
  });
  test("should display the chips color according to the chipColors attribute", async () => {
    const wrapper = mthGridWrapper(headers, items);
    await flushPromises();

    // get 'green' v-chip
    let chip = wrapper.findAll("tbody .v-chip")[0];
    // should be green
    expect(chip.classes()).toContain("text-green");
    // get 'nothing' v-chip
    chip = wrapper.findAll("tbody .v-chip")[1];
    // should not be colored
    expect(chip.classes()).not.toContain("text");
    // get 'red' v-chip
    chip = wrapper.findAll("tbody .v-chip")[2];
    // should be red
    expect(chip.classes()).toContain("text-red");
  });

  test("should display an editable column", async () => {
    const wrapper = mthGridWrapper(headers, items);
    await flushPromises();

    // count mdi-pencil and inputs
    expect(wrapper.findAll(".mdi-pencil").length).equals(items.length);
    expect(wrapper.findAll("table input[type='text']").length).equals(
      items.length
    );
  });
  test("should edit the value in the column", async () => {
    const wrapper = mthGridWrapper(headers, items, "", true, "all");
    await flushPromises();

    // column is an editable input
    const input = wrapper.findAll("table input[type='text']")[0];
    await input.setValue("edited value");

    // select the line
    wrapper.findAll("table input[type='checkbox']")[1].trigger("click");
    // check that MthGridItem has the modifications
    expect(
      (
        wrapper.findAll("tbody td input[type='text']")[0]
          .element as HTMLInputElement
      ).value
    ).toEqual("edited value");
  });
  test("should display a column with v-html", async () => {
    const wrapper = mthGridWrapper(headers, items);
    await flushPromises();

    // v html is displayed in a span
    expect(wrapper.findAll(".v-html-column").length).equals(items.length);
  });
});

describe("MthGrid.vue ------ lines checking", () => {
  test("should display no checkboxes for default select strategy", async () => {
    const wrapper = mthGridWrapper(headers, items);
    await flushPromises();

    // default value for prop 'selectStrategy' should be 'undefined'
    expect(wrapper.props("selectStrategy")).toBe(undefined);
    // there should 1 additional checkbox to select all
    expect(wrapper.findAll("input[type='checkbox']").length).toBe(0);
  });
  test("should display checkboxes for other select strategy", async () => {
    const wrapper = mthGridWrapper(headers, items, "", true, "single");
    await flushPromises();

    // value for prop 'selectStrategy' should be 'single'
    expect(wrapper.props("selectStrategy")).toBe("single");
    // there should as many checkbox as items to select one
    expect(wrapper.findAll("input[type='checkbox']").length).toBe(items.length);
  });
});

describe("MthGrid.vue ------ research bar", () => {
  test("should display a research bar along with the grid by default", async () => {
    const wrapper = mthGridWrapper(headers, items);
    await flushPromises();

    expect(wrapper.find("[data-testid='globalSearch']").exists()).toBe(true);
  });
  test("should not display the research bar if search is set to false", async () => {
    const wrapper = mthGridWrapper(headers, items, "", false);
    await flushPromises();

    expect(wrapper.find("[data-testid='globalSearch']").exists()).toBe(false);
  });
  test("writing in search bar with exact search should update grid content", async () => {
    const wrapper = mthGridWrapper(headers, items);
    await flushPromises();

    // mock the writing in search bar
    wrapper.getComponent(MthSearchBar).vm.$emit("search", {
      searchInput: "mario",
      searchType: "EXACT",
    });
    await flushPromises();
    // count rows
    expect(wrapper.findAll("tbody > tr").length).equals(2);
  });
  test("writing in a search bar with fuzzy search should update grid content", async () => {
    const wrapper = mthGridWrapper(headers, items, "GLOBAL");
    await flushPromises();

    // mock the writing in a search bar
    wrapper.getComponent(MthSearchBar).vm.$emit("search", {
      searchInput: "nale",
      searchType: "FUZZY",
    });
    await flushPromises();
    // count rows
    expect(wrapper.findAll("tbody > tr").length).equals(2);
  });
});

describe("MthGrid.vue ------ select items", () => {
  test("should emit an event when an item has been selected", async () => {
    const wrapper = mthGridWrapper(headers, items, "", true, "all");
    await flushPromises();

    // select the line
    await wrapper.findAll("table input[type='checkbox']")[2].setChecked(); // + global checkbox

    expect(wrapper.findAll(".mdi-checkbox-marked").length).toBe(1);
    // check that the event has been emitted
    expect(wrapper.emitted("selection")).toBeTruthy();
    // check that the event has been emitted with the right item
    expect(wrapper.emitted("selection")![0][0]).toEqual([items[1]]);
  });
});
