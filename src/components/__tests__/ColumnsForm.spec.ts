import { describe, expect, test } from "vitest";
import { mount } from "@vue/test-utils";
import { createVuetify } from "vuetify";
import ColumnsForm from "../ColumnsForm.vue";
import { MthGridHeader } from "@/types/MthGridHeader";

function columnsFormdWrapper(headers: MthGridHeader[]) {
  const vuetify = createVuetify();
  return mount(ColumnsForm, {
    global: {
      plugins: [vuetify],
    },
    props: {
      headers,
    },
  });
}

const headers: MthGridHeader[] = [
  { title: "Col 1", key: "col1" },
  { title: "Col 2", key: "col2" },
];

describe("ColumnsForm.vue", () => {
  test("should display a handle and an input for each header", () => {
    const wrapper = columnsFormdWrapper(headers);

    // count handles and inputs
    expect(wrapper.findAll(".handle").length).toEqual(headers.length);
    expect(wrapper.findAll("[data-testid='titleInput']").length).toEqual(
      headers.length
    );
    expect(
      (
        wrapper.findAll("[data-testid='titleInput'] input")[0]
          .element as HTMLInputElement
      ).value
    ).toContain(headers[0].title);
    expect(
      (
        wrapper.findAll("[data-testid='titleInput'] input")[1]
          .element as HTMLInputElement
      ).value
    ).toContain(headers[1].title);
  });

  test("should emit an event when clicking on 'close'", async () => {
    const wrapper = columnsFormdWrapper(headers);

    // click on 'close'
    await wrapper.find("[data-testid='closeBtn']").trigger("click");

    // should have emitted event
    expect(wrapper.emitted("close")).toBeTruthy();
  });

  test("should emit an event when clicking on 'save'", async () => {
    const wrapper = columnsFormdWrapper(headers);

    // click on 'save'
    await wrapper.find("[data-testid='saveBtn']").trigger("click");

    // should have emitted event
    expect(wrapper.emitted("save")).toBeTruthy();
    expect(wrapper.emitted("save")![0][0]).toEqual(headers);
  });
});
