import { describe, expect, test } from "vitest";
import { mount } from "@vue/test-utils";
import { createVuetify } from "vuetify";
import GridModifyHeaders from "../GridModifyHeaders.vue";
import { MthGridHeader } from "@/types/MthGridHeader";

function columnsFormdWrapper(headers: MthGridHeader[]) {
  const vuetify = createVuetify();
  return mount(GridModifyHeaders, {
    global: {
      plugins: [vuetify],
    },
    props: {
      headers,
      dark: false,
      modelValue: headers
    },
  });
}

const headers: MthGridHeader[] = [
  { title: "Col 1", key: "col1", selected: true },
  { title: "Col 2", key: "col2", selected: true },
];

describe("GridModifyHeaders.vue", () => {
  test("should render", () => {
    const wrapper = columnsFormdWrapper(headers);

    // v-select
    expect(wrapper.find("[data-testid='selectHeaders']").exists).toBeTruthy();
    // button to open ColumnsForm
    expect(wrapper.find("[data-testid='parametersBtn']").exists).toBeTruthy();
  });

  test("should display all the headers in the vselect", () => {
    const wrapper = columnsFormdWrapper(headers);

    // get v-select
    const selectHeaders = wrapper.find("[data-testid='selectHeaders']");
    expect(selectHeaders.findAll(".v-select__selection").length).toEqual(
      headers.length
    );
    selectHeaders.findAll(".v-select__selection-text").forEach((t, i) => {
      expect(t.element.textContent).toContain(headers[i].title);
    });
  });
});
