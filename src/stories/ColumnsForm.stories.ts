import { MthGridHeader } from "@/types/MthGridHeader";
import ColumnsForm from "../components/ColumnsForm.vue";
import { Meta, StoryObj } from "@storybook/vue3";
import { action } from "@storybook/addon-actions";

const meta: Meta<typeof ColumnsForm> = {
  title: "Mth-grids/ColumnsForm",
  component: ColumnsForm,
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof ColumnsForm>;

const headers: MthGridHeader[] = [
  {
    title: "Country",
    key: "ctry",
    sortable: true,
    searchable: false,
  },
  {
    title: "Capital",
    key: "cptl",
    sortable: true,
    searchable: true,
  },
];

export const Default: Story = {
  render: (args) => ({
    components: { ColumnsForm },
    setup() {
      return {
        ...args,
        close: action("close"),
        save: action("save"),
      };
    },
    template: "<ColumnsForm :headers='headers' @close='close' @save='save' />",
  }),
  args: {
    headers: headers,
  },
};
