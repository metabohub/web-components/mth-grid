import { MthGridHeader } from "@/types/MthGridHeader";
import GridModifyHeaders from "../components/GridModifyHeaders.vue";
import { Meta, StoryObj } from "@storybook/vue3";
import { action } from "@storybook/addon-actions";

const meta: Meta<typeof GridModifyHeaders> = {
  title: "Mth-grids/GridModifyHeaders",
  component: GridModifyHeaders,
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof GridModifyHeaders>;

const headers: MthGridHeader[] = [
  {
    title: "Country",
    key: "ctry",
    sortable: true,
    searchable: false,
  },
  {
    title: "Capital",
    key: "cptl",
    sortable: true,
    searchable: true,
    selected: false,
  },
];
const headersVSelectModel: MthGridHeader[] = headers.filter((h) => h.selected != false);

export const Default: Story = {
  render: (args) => ({
    components: { GridModifyHeaders },
    setup() {
      return {
        ...args,
        updateHeaders: action("updateHeaders"),
      };
    },
    template: "<GridModifyHeaders :modelValue :headers :dark @updateHeaders='updateHeaders' />",
  }),
  args: {
    headers,
    modelValue: headersVSelectModel,
    dark: false
  },
};
