import { SelectStrategy } from "./../types/SelectStrategy";
import type { Meta, StoryObj } from "@storybook/vue3";
import { action } from "@storybook/addon-actions";
import MthGrid from "../components/MthGrid.vue";
import type { MthGridHeader } from "../types/MthGridHeader.js";
import type { MthGridItem } from "../types/MthGridItem.js";

const meta: Meta<typeof MthGrid> = {
  title: "Mth-grids/MthGrid",
  component: MthGrid,
  tags: ["autodocs"],
  argTypes: {
    selectStrategy: {
      control: "select",
      options: ["all", "pages", "single"],
    },
  },
};

export default meta;
type Story = StoryObj<typeof MthGrid>;

const headers: MthGridHeader[] = [
  {
    title: "Country",
    key: "ctry",
    sortable: true,
    searchable: false,
  },
  {
    title: "Capital",
    key: "cptl",
    sortable: true,
    searchable: true,
  },
];
const items: MthGridItem[] = [
  { ctry: "France", cptl: "Paris" },
  { ctry: "Germany", cptl: "Berlin" },
];
const selectStrategy: SelectStrategy = "all";

export const Default: Story = {
  render: (args) => ({
    components: { MthGrid },
    setup() {
      return {
        ...args,
        selection: action("selection"),
      };
    },
    template:
      "<MthGrid :headers :items :isLoading :selectStrategy @selection='selection' />",
  }),
  args: {
    headers,
    items,
    isLoading: false,
    selectStrategy,
  },
};

export const WithSlot: Story = {
  render: (args) => ({
    components: { MthGrid },
    setup() {
      return {
        ...args,
        selection: action("selection"),
      };
    },
    template:
      "<MthGrid :headers :items :isLoading :selectStrategy @selection='selection'>" +
      "<template #actions>" +
      "<button>Display informations</button>" +
      "</template>" +
      "</MthGrid>",
  }),
  args: {
    headers,
    items,
    isLoading: false,
    selectStrategy,
  },
};

export const ChipColumn: Story = {
  render: (args) => ({
    components: { MthGrid },
    setup() {
      return {
        ...args,
        selection: action("selection"),
      };
    },
    template:
      "<MthGrid :headers :items :isLoading :selectStrategy @selection='selection' />",
  }),
  args: {
    headers: [
      {
        title: "Country",
        key: "ctry",
        sortable: true,
        searchable: false,
      },
      {
        title: "Capital",
        key: "cptl",
        sortable: true,
        searchable: true,
      },
      {
        title: "Boolean",
        key: "bool",
        chip: true,
        chipColors: {
          true: "green",
          false: "red",
        },
      },
    ],
    items: [
      { ctry: "France", cptl: "Paris", bool: "true" },
      { ctry: "Germany", cptl: "Berlin", bool: "false" },
      { ctry: "Spain", cptl: "Madrid", bool: "hello world" },
    ],
    isLoading: false,
    selectStrategy,
  },
};

export const EditableColumn: Story = {
  render: (args) => ({
    components: { MthGrid },
    setup() {
      return {
        ...args,
        selection: action("selection"),
      };
    },
    template:
      "<MthGrid :headers :items :isLoading :selectStrategy @selection='selection' />",
  }),
  args: {
    headers: [
      {
        title: "Country",
        key: "ctry",
        sortable: true,
        searchable: false,
      },
      {
        title: "Capital",
        key: "cptl",
        sortable: true,
        searchable: true,
        editable: true,
      },
    ],
    items: [
      { ctry: "France", cptl: "Paris" },
      { ctry: "Germany", cptl: "Berlin" },
    ],
    isLoading: false,
    selectStrategy,
  },
};

export const VHtmlColumn: Story = {
  render: (args) => ({
    components: { MthGrid },
    setup() {
      return {
        ...args,
        selection: action("selection"),
      };
    },
    template:
      "<MthGrid :headers :items :isLoading :selectStrategy @selection='selection' />",
  }),
  args: {
    headers: [
      {
        title: "Country",
        key: "ctry",
        sortable: true,
        searchable: false,
      },
      {
        title: "Capital",
        key: "cptl",
        sortable: true,
        searchable: true,
        vHtml: true,
      },
    ],
    items: [
      { ctry: "France", cptl: "<b>Paris</b>" },
      { ctry: "Germany", cptl: "<i>Berlin</i>" },
    ],
    isLoading: false,
    selectStrategy,
  },
};

export const SelectedColumn: Story = {
  render: (args) => ({
    components: { MthGrid },
    setup() {
      return {
        ...args,
        selection: action("selection"),
      };
    },
    template:
      "<MthGrid :headers :items :isLoading :selectStrategy @selection='selection' />",
  }),
  args: {
    headers: [
      {
        title: "Country",
        key: "ctry",
        sortable: true,
        searchable: false,
      },
      {
        title: "Capital",
        key: "cptl",
        sortable: true,
        searchable: true,
        selected: false,
      },
    ],
    items: [
      { ctry: "France", cptl: "Paris" },
      { ctry: "Germany", cptl: "Berlin" },
    ],
    isLoading: false,
    selectStrategy,
  },
};
