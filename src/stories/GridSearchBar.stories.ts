import GridSearchBar from "../components/GridSearchBar.vue";
import { Meta, StoryObj } from "@storybook/vue3";
import { action } from "@storybook/addon-actions";

const meta: Meta<typeof GridSearchBar> = {
  title: "Mth-grids/GridSearchBar",
  component: GridSearchBar,
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof GridSearchBar>;

const searchableHeadersTitles = ["Ids", "Names"];

export const Default: Story = {
  render: (args) => ({
    components: { GridSearchBar },
    setup() {
      return {
        ...args,
        submit: action("submit"),
      };
    },
    template: "<GridSearchBar :searchableHeadersTitles :dark @submit='submit' />",
  }),
  args: {
    searchableHeadersTitles,
    dark: false
  },
};
