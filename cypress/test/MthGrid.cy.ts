import MthGrid from "@/components/MthGrid.vue";
import { MthGridHeader } from "@/types/MthGridHeader";
import { MthGridItem } from "@/types/MthGridItem";
import vuetify from "@/plugins/vuetify";

const headers: MthGridHeader[] = [
  { title: "col1", key: "id", searchable: false },
  { title: "col2", key: "name", searchable: true, editable: true },
  {
    title: "col3",
    key: "bool",
    searchable: false,
    chip: true,
    chipColors: { red: "red", green: "green" },
  },
  { title: "col4", key: "col4", searchable: true },
  { title: "col5", key: "tags", searchable: false, vHtml: true }
];
const items: MthGridItem[] = [
  { id: "Id1", name: "name1", bool: "green", col4: "mario", tags: "<i>italic</i>" },
  { id: "mario", name: "name2", bool: "nothing", col4: "luigi", tags: "<b>bold</b>" },
  { id: "Id3", name: "mario", bool: "red", col4: "peach", tags: "CO<sub>2</sub>" },
];

describe("<MthGrid />", () => {
  beforeEach(() => {
    cy.viewport(1800, 1000);
  });

  it("should render", () => {
    // see: https://on.cypress.io/mounting-vue
    cy.mount(MthGrid, {
      global: {
        plugins: [vuetify],
      },
      props: {
        headers,
        items,
      },
    });

    // headers
    headers.forEach((h) => {
      cy.contains("th", h.title);
    });

    // items
    // input value
    cy.get("td > input").each(($el, index: number) => {
      expect($el).to.have.value(items[index].name);
    });
    items.forEach((i) => {
      cy.contains("td", i.id);
      cy.contains(".v-chip", i.bool);
    });
  });


  it("should render the v-html columns", () => {
    // see: https://on.cypress.io/mounting-vue
    cy.mount(MthGrid, {
      global: {
        plugins: [vuetify],
      },
      props: {
        headers,
        items,
      },
    });

    // v-html columns should not contain < or >
    cy.get(".v-html-column").each(($el) => {
      expect($el).not.to.contain("<");
      expect($el).not.to.contain(">");
    });
  });

  it("selecting headers should update the table's headers", () => {
    cy.stub(ResizeObserver);

    cy.mount(MthGrid, {
      global: {
        plugins: [vuetify],
      },
      props: {
        headers,
        items,
      },
    });

    // unselect header 1
    cy.get("[data-testid='selectHeaders']").parent().click();
    cy.get(".v-menu").contains(headers[0].title).click();

    // the header should not be in the table anymore
    cy.contains("th", headers[0].title).should("not.exist");
  });

  it("renaming a column should update the table's headers", () => {
    cy.mount(MthGrid, {
      global: {
        plugins: [vuetify],
      },
      props: {
        headers,
        items,
      },
    });

    // click on column parameters
    cy.get("[data-testid='parametersBtn']").click();

    // ColumnsForm should be open
    cy.get("[data-testid='columnsForm']").should("be.visible");

    // change a column's name and save
    cy.get("[data-testid='columnsForm'] input").first().clear({ force: true }); // element not visible
    cy.get("[data-testid='columnsForm'] input").first().type("Hello");
    cy.get("[data-testid='saveBtn']").click({ force: true }); // element not visible

    // check if headers changed
    cy.contains("th", "Hello");
  });

  it("searching 'All' should filter the table", () => {
    cy.mount(MthGrid, {
      global: {
        plugins: [vuetify],
      },
      props: {
        headers,
        items,
      },
    });

    // search for a value
    cy.get("[data-testid='globalSearch']").type("peach");

    // only the row with the searched value should be displayed
    cy.get("tbody tr").should("have.length", 1);
    cy.get("tbody td").contains("peach");
  });

  it("should search on a single column", () => {
    cy.mount(MthGrid, {
      global: {
        plugins: [vuetify],
      },
      props: {
        headers,
        items,
      },
    });

    // select the column to search on
    cy.get("[data-testid='searchVSelect']").click();
    // select the 'col2' column
    cy.get(".v-menu").contains(headers[1].title).click();
    // cy.get("[data-testid='searchVSelect'] .v-list-item").contains(headers[1].key).click();

    // search for a value
    cy.get("[data-testid='globalSearch']").type("peach");

    // should not find anything
    cy.get("tbody tr").should("have.length", 1);
    cy.get("tbody td").contains("No data available");

    // select the 'col4' column
    cy.get("[data-testid='searchVSelect']").click();
    cy.get(".v-menu").contains(headers[3].title).click();
    // cy.get("[data-testid='searchVSelect']").contains(headers[3].key).click();

    // only the row with the searched value should be displayed
    cy.get("tbody tr").should("have.length", 1);
    cy.get("tbody td").contains("peach");
  });

  it("should select shift multiple ligns", () => {
    cy.mount(MthGrid, {
      global: {
        plugins: [vuetify],
      },
      props: {
        headers,
        items,
        selectStrategy: "all",
      },
    });

    // select the first row
    cy.get("table input[type='checkbox']").eq(1).click();
    // shift + click on the last row
    cy.get("table input[type='checkbox']").last().click({ shiftKey: true });
    // cy.get("table input[type='checkbox']").last().type("{shift}", { release: false });
    // cy.get("table input[type='checkbox']").last().click();

    // all rows should be selected
    cy.get(".mdi-checkbox-marked").should("have.length", items.length + 1); // + global checkbox
  });

  // TODO : test unselect shift
  // it("should unselect shift multiple ligns", () => {
  //   cy.mount(MthGrid, {
  //     global: {
  //       plugins: [vuetify],
  //     },
  //     props: {
  //       headers,
  //       items,
  //       selectStrategy: "all",
  //     },
  //   });

  //   // select everyone
  //   cy.get("table input[type='checkbox']").first(0).click();
  //   cy.get(".mdi-checkbox-marked").should("have.length", items.length + 1); // + global checkbox

  //   // unselect shift
  //   cy.get("table input[type='checkbox']").eq(1).click({ shiftKey: true });

  //   cy.get(".mdi-checkbox-marked").should("have.length", 0);
  // });
});
